/* On inclut l'interface publique */
#include "mem.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>

/* Définition de l'alignement recherché
 * Avec gcc, on peut utiliser __BIGGEST_ALIGNMENT__
 * sinon, on utilise 16 qui conviendra aux plateformes qu'on cible
 */
#ifdef __BIGGEST_ALIGNMENT__
#define ALIGNMENT __BIGGEST_ALIGNMENT__
#else
#define ALIGNMENT 16
#endif

/* structure placée au début de la zone de l'allocateur

   Elle contient toutes les variables globales nécessaires au
   fonctionnement de l'allocateur

   Elle peut bien évidemment être complétée
*/
struct allocator_header {
        size_t memory_size;
	mem_fit_function_t *fit;
	struct fb *first_fb;
};

/* La seule variable globale autorisée
 * On trouve à cette adresse le début de la zone à gérer
 * (et une structure 'struct allocator_header)
 */
static void* memory_addr;

static inline void *get_system_memory_addr() {
	return memory_addr;
}

static inline struct allocator_header *get_header() {
	struct allocator_header *h;
	h = get_system_memory_addr();
	return h;
}

static inline size_t get_system_memory_size() {
	return get_header()->memory_size;
}


struct fb {
	size_t size;
	struct fb* next;
};

struct ob {
	size_t size;
};


void mem_init(void* mem, size_t taille)
{
	printf("sizeof(fb) = %ld\nsizeof(ob) = %ld\nsizeof(allocator_header) = %ld\n",sizeof(struct fb),sizeof(struct ob),sizeof(struct allocator_header));
    memory_addr = mem;
    *(size_t*)memory_addr = taille;
	/* On vérifie qu'on a bien enregistré les infos et qu'on
	 * sera capable de les récupérer par la suite
	 */
	assert(mem == get_system_memory_addr());
	assert(taille == get_system_memory_size());
	struct fb *bloc;
	bloc=mem+sizeof(struct allocator_header);
	bloc->size = taille-sizeof(struct allocator_header);
	bloc->next = NULL;
	get_header()->first_fb=bloc;
	mem_fit(&mem_fit_first);
}

void mem_show(void (*print)(void *, size_t, int)) {
	/* ... */
	while (/* ... */ 0) {
		/* ... */
		print(/* ... */NULL, /* ... */0, /* ... */0);
		/* ... */
	}
}

void mem_fit(mem_fit_function_t *f) {
	get_header()->fit = f;
}

void *mem_alloc(size_t taille) {
	struct fb *ptr = get_header()->first_fb;	//On recup le premier fb

	__attribute__((unused)) /* juste pour que gcc compile ce squelette avec -Werror */
	struct fb *fb=get_header()->fit(ptr,taille + sizeof(struct ob));
	if (fb == NULL)
		return NULL;
	
	struct fb *newfb;
	struct ob *newob;
	newfb = (void *)fb + sizeof(struct ob) + taille;
	newfb->size = fb->size - sizeof(struct ob) - taille;
	newfb->next = fb->next;
	newob = (struct ob*)fb;
	newob->size = taille;
	return (newob + sizeof(struct ob));
}


void mem_free(void* mem) {
}


struct fb* mem_fit_first(struct fb *list, size_t size) {	
	while (list != NULL && list->size < size){	//On scroll dans nos fb tant que la taille n'est pas suffisante
		list = list->next;
	}
	return list;
}

/* Fonction à faire dans un second temps
 * - utilisée par realloc() dans malloc_stub.c
 * - nécessaire pour remplacer l'allocateur de la libc
 * - donc nécessaire pour 'make test_ls'
 * Lire malloc_stub.c pour comprendre son utilisation
 * (ou en discuter avec l'enseignant)
 */
size_t mem_get_size(void *zone) {
	/* zone est une adresse qui a été retournée par mem_alloc() */

	/* la valeur retournée doit être la taille maximale que
	 * l'utilisateur peut utiliser dans cette zone */
	return ((struct ob*)(zone-sizeof(struct ob)))->size;
}

/* Fonctions facultatives
 * autres stratégies d'allocation
 */
struct fb* mem_fit_best(struct fb *list, size_t size) {
	struct fb *minfb = mem_fit_first(list, size);	// On initialise la structure "minfb" au premier match de minfb.size > size (mem_fit_first)

	list = minfb;		// Notre pointeur qui va nous permettre de scroll dans la liste, on l'initialise a minfb pour optimiser
	while (list != NULL){
		if (list->size > size && minfb->size > list->size)	// Si min > ptr->size et ptr->size > taille ( En gros si on trouve une valeur inférieure à notre min qui est valable )
			minfb = list;
		list = list->next;
	}
	return minfb;
}

struct fb* mem_fit_worst(struct fb *list, size_t size) {
	struct fb *maxfb = mem_fit_first(list, size);	// On initialise la structure "maxfb" au premier match de maxfb.size > size (mem_fit_first)

	list = maxfb;		// Notre pointeur qui va nous permettre de scroll dans la liste, on l'initialise a maxfb pour optimiser
	while (list != NULL){
		if (list->size > size && maxfb->size < list->size)	// Si max < ptr->size et ptr->size > taille ( En gros si on trouve une valeur suppérieure à notre max qui est valable )
			maxfb = list;
		list = list->next;
	}
	return maxfb;
}
